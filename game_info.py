import click
import requests
from prettytable import PrettyTable


@click.command()
@click.option('--game', prompt='The Name of the game or a Substring of it',
              help='The name of the game you want sale information about.')
def input_command(game):
    game_ids = get_app_id(game)
    game_meta_data = []
    for id in game_ids:
        game_meta_data.append(get_game_meta_data(id))
    table = create_pretty_tables_from_list_of_dicts(game_meta_data)
    print('The first 10 matches for your query are:\n')
    print(table)
    input('Press Enter to exit')


def get_app_id(name):
    response = requests.get('https://steamcommunity.com/actions/SearchApps/%s' % name)
    id_matches = []
    for app in response.json():
        id_matches.append(app['appid'])
    return id_matches


def get_game_meta_data(id):
    game_metadata = {}
    response = requests.get('https://store.steampowered.com/api/appdetails?appids=%s&cc=de' % id)
    game_metadata['name'] = response.json()[str(id)]['data']['name']
    game_metadata['AppID'] = id
    game_metadata['type'] = response.json()[str(id)]['data']['type']
    try:
        game_metadata['price'] = response.json()[str(id)]['data']['price_overview']['final_formatted']
    except KeyError:
        game_metadata['price'] = 'Unknown'
    finally:
        return game_metadata


def create_pretty_tables_from_list_of_dicts(list_of_dict):
    table = PrettyTable()
    table.field_names = ["Name", "AppID", "Type", "Price"]
    for game in list_of_dict:
        table.add_row([game['name'], game['AppID'], game['type'], game['price']])
    return table


def test_get_game_meta_data():
    # prepare
    expected_output_1 = {'name': 'Call of Duty®: Modern Warfare® II', 'AppID': 1938090, 'type': 'game',
                         'price': '69,99€'}
    expected_output_2 = {'name': 'Grand Theft Auto V', 'AppID': 271590, 'type': 'game', 'price': 'Unknown'}

    # call
    output_1 = get_game_meta_data(1938090)
    output_2 = get_game_meta_data(271590)

    # assert
    assert output_1 == expected_output_1
    assert output_2 == expected_output_2


def test_get_app_ids():
    # prepare
    expected_output = ['1938090', '311210', '518790', '400750', '485440', '42700', '672680', '10090', '42680', '7940']

    # call
    output = get_app_id('Call')

    # assert
    assert output == expected_output


if __name__ == '__main__':
    input_command()


