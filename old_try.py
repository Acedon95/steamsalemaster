import requests
import click
import json
from datetime import datetime

#auth = {'x-webapi-key': '21625A982C1A2725D5E3C1F24EB9D93F'}
b = requests.get('https://steamcommunity.com/market/pricehistory/?key=21625A982C1A2725D5E3C1F24EB9D93F&steamid=76561198029874992&appid=440&market_hash_name=Tour%20of%20Duty%20Ticket')
print(b.json())


@click.command()
@click.option('--game_id', prompt='The AppId of the game', help='The name of the game you want sale information about.')
def input_command(game_id):
    game_name = get_game_name(game_id)
    # price_history_json = get_price_history(game-id, game_name)
    price_history_json = get_price_history2()
    sale_days, sale_years = count_sale_days(price_history_json)
    print("Since %s, the game %s been on sale for %s day." % (sale_years[0], game_name, len(sale_days)))
    print("On average the game has been on sale %s days a year." % (len(sale_days) / len(sale_years)))
    print("The game has been on sale for %s years." % len(sale_years))


def get_game_name(id):
    response = requests.get('https://store.steampowered.com/api/appdetails?appids=%s' % id)
    return response.json()[id]['data']['name']


def get_price_history(appid, market_hash_name):
    url = 'https://steamcommunity.com/market/pricehistory/?appid=%s&market_hash_name=%s' % (appid, market_hash_name)
    return requests.get(url).json()


def get_price_history2():
    with open('test_response.json', 'r') as f:
        return json.load(f)


def convert_month(month):
    months = {
        'jan': 1,
        'feb': 2,
        'mar': 3,
        'apr': 4,
        'may': 5,
        'jun': 6,
        'jul': 7,
        'aug': 8,
        'sep': 9,
        'oct': 10,
        'nov': 11,
        'dec': 12
    }
    return str(months[month.lower()])


def count_sale_days(price_history):
    dates = []
    tmp_year = None
    years = []
    for day in price_history['prices']:
        date_list = day[0].split(':')[0].split(' ')
        month = convert_month(date_list[0])  # month
        day = date_list[1]  # day
        year = date_list[2]  # year
        date = datetime.strptime('%s/%s/%s' % (month, day, year), '%m/%d/%Y')
        if date not in dates:
            dates.append(date)
        if tmp_year is None or tmp_year != year:
            tmp_year = year
        if tmp_year not in years:
            years.append(tmp_year)

    return dates, years


def test_count_sale_days():
    # prepare
    with open('test_response.json', 'r') as f:
        response = json.load(f)

    # call
    result_day, result_years = count_sale_days(response)

    # assert
    assert result_day == 1500
    assert result_years == 5


def test_get_game_name():
    # prepare
    appid = '440'

    # call
    result = get_game_name(appid)

    # assert
    assert result == 'Team Fortress 2'


if __name__ == '__main__':
    input_command()
